import hue
import argparse
from pathlib import Path
from concurrent.futures import ThreadPoolExecutor

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Rotate hue of an image')
    parser.add_argument('filename', type=Path)
    parser.add_argument('smoothness', type=int)
    parser.add_argument('outputpath', type=Path, default=Path('./hue_wallpapers'), nargs='?')
    args = parser.parse_args()

    hue_distance = 360 / (args.smoothness)

    args.outputpath.mkdir(parents=True,exist_ok=True)

    # Multithreading
    executor = ThreadPoolExecutor(max_workers=8)
    futures = []
    

    for i in range(args.smoothness):
        filename = str(i).zfill(3)+".png"
        # hue.create_image_file(args.filename, i*hue_distance, str(args.outputpath / filename))
        a = executor.submit(hue.create_image_file, args.filename, i*hue_distance, str(args.outputpath / filename))
        futures.append(a)
