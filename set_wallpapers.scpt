#!/usr/bin/env osascript

-- Set wallpaper on all screens and virtual desktops, doesnt seem to be working
-- https://apple.stackexchange.com/questions/338131/applescript-get-every-desktop-including-hidden-mission-control-desktops

-- Path to current script file
-- https://github.com/btcrooks/Scriptlets/blob/master/Applescript/01-current-path.scpt

-- File chooser dialog
-- https://stackoverflow.com/questions/33716620/getting-the-name-of-a-chosen-file-in-applescript?rq=1

-- Assumes wallpapers are in ./hue-wallpapers/
tell application "Finder" to set folderpath to (POSIX path of (container of (path to me) as string)) & "hue_wallpapers/"

-- Lower level wallpaper changing
-- https://apple.stackexchange.com/questions/433794/seamlessly-change-all-desktop-spaces-wallpaper-on-mac-without-killall-dock?rq=1


-- Reset wallpaper folder to default
tell application "System Events"
	tell every desktop
		set pictures folder to "/Library/Desktop Pictures/" -- Default wallpapers
	end tell
end tell

-- flickering, but at least it resets the current picture to start with the same on multiple monitors
-- I guess the previous folder has to be set for a minimum amount of time
-- Could be buggy https://apple.stackexchange.com/questions/276046/what-is-the-minimum-delay-in-applescript#276053
-- .4 seems to be a good value (experimental)
delay .4

-- Does not seem necessary
-- tell application "Terminal"
-- 	do shell script "killall Dock"
-- end tell

tell application "System Events"
	-- RANDOM ROTATION OF A FOLDER OF IMAGES
	tell every desktop
		set picture rotation to 1 -- (0=off, 1=interval, 2=login, 3=sleep)
		set random order to false
		set pictures folder to folderpath
		set change interval to .5 -- seconds
	end tell
end tell
