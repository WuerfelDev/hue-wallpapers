#!/usr/bin/env osascript

--  Inspired by https://apple.stackexchange.com/a/333708
property displayCount : missing value
property tempDisplayCount : missing value

-- initialize
countDisplays()
copy tempDisplayCount to displayCount

repeat
	countDisplays()
	-- Run an action if another display is detected
	if tempDisplayCount is greater than displayCount then
		displayConnected()
	end if
	-- Adjust counter after a display change
	if tempDisplayCount is not equal to displayCount then
		copy tempDisplayCount to displayCount
	end if
end repeat


on displayConnected()
	run script "./set_wallpapers.scpt"
end displayConnected


on countDisplays()
	tell application "Image Events"
		set theDisplays to count of displays
	end tell
	set tempDisplayCount to theDisplays
	delay 4 -- How Often To Check How Many Connected Monitors.. In Seconds
end countDisplays
